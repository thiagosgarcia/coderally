import com.ibm.coderally.api.AIUtils;
import com.ibm.coderally.api.DefaultCarAI;
import com.ibm.coderally.entity.cars.Car;
import com.ibm.coderally.entity.obstacle.Obstacle;
import com.ibm.coderally.geo.CheckPoint;
import com.ibm.coderally.track.Track;

public class Roadrunner extends DefaultCarAI {

	public Roadrunner() {
	}

	@Override
	public void onRaceStart() {
		// Provide custom logic or remove method for default implementation.

		getCar().setBrakePercent(0);
		getCar().setTarget(
				AIUtils.getClosestLane(getCar().getCheckpoint(), getCar()
						.getPosition()));
		getCar().setAccelerationPercent(100);
	}

	@Override
	public void onCheckpointUpdated(CheckPoint oldCheckpoint) {
		// Provide custom logic or remove method for default implementation.

		getCar().setTarget(
				AIUtils.getClosestLane(getCar().getCheckpoint(), getCar()
						.getPosition()));
		
		double heading = getCar().calculateHeading(getCar().getTarget());
		if(heading > 90){
			getCar().setBrakePercent(0);
			getCar().setAccelerationPercent(0);
		}else if(heading > 60){ 
			getCar().setBrakePercent(0);
			getCar().setAccelerationPercent(0);
		}else if(heading > 45){
			getCar().setBrakePercent(0);
			getCar().setAccelerationPercent(0);
		}else if(heading > 10){
			getCar().setBrakePercent(0);
			getCar().setAccelerationPercent(20);
		}else if(heading > 3){
			getCar().setBrakePercent(0);
			getCar().setAccelerationPercent(50);
		}
		
		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
	}

	@Override
	public void onOffTrack() {
		getCar().setBrakePercent(100);
		getCar().setAccelerationPercent(0);
		getCar().setTarget(
				getCar().getCheckpoint().getIntersectionPoint(
						getCar().getRotation(), getCar().getPosition()));
	}

	@Override
	public void onCarCollision(Car other) {
		if (getCar().getTarget().equals(other.getTarget())) {
			getCar().setTarget(
					AIUtils.getAlternativeLane(getCar().getCheckpoint(),
							getCar().getPosition()));
		}
		getCar().setAccelerationPercent(0);
		getCar().setBrakePercent(50);
	}

	@Override
	public void onOpponentInProximity(Car car) {
		getCar().setTarget(
				AIUtils.getAlternativeLane(getCar().getCheckpoint(), getCar()
						.getPosition()));
	}

	@Override
	public void onObstacleInProximity(Obstacle obstacle) {
		getCar().setTarget(
				AIUtils.getAlternativeLane(getCar().getCheckpoint(), getCar()
						.getPosition()));
	}

	@Override
	public void onTimeStep() {
		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
	}

	@Override
	public void onStalled() {
		getCar().setTarget(
				AIUtils.getClosestLane(getCar().getCheckpoint(), getCar()
						.getPosition()));
		getCar().setBrakePercent(0);
		getCar().setAccelerationPercent(100);
	}

	@Override
	public void onObstacleCollision(Obstacle obstacle) {
		if (getCar().getTarget().equals(obstacle.getPosition())) {
			getCar().setTarget(
					AIUtils.getAlternativeLane(getCar().getCheckpoint(),
							getCar().getPosition()));
		}
		getCar().setAccelerationPercent(100);
		getCar().setBrakePercent(0);
	}

	@Override
	public void init(Car car, Track track) {
		super.init(car, track);
		// TODO Set initializer for all the variables and properties I need to
		// use
	}

	static class CarUtil {
		static float getSpeed(Car car) {
			return car.getVelocity().normalize() * 1.609344F;
		}
	}

}
